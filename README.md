# Flexitime

Flexible day-to-day activity planning.

## Overview

Flexitime is intended to provide a simple way to track day-to-day activities
hour-by-hour by keeping track of what you're currently working on and in real time
tracking if you've over (or under) estimated the time required for an activity to
be completed. 

## Goals

* Simple, minimalist, intuitive design (no need for fancyness though)
   - The timeline should be the view where you spend 95% of the time
     and most day-to-day functionality should be contained in it.
     (E.g. rescuetime)
* Quick to use, creating a new activity or other simple action should
  be very fast (performance + UI wise).
* Accessibile
* Responsive (Mobile friendly)
* Extra generated information (reports, etc.) shouldn't get in the users way
  performance or UI wise.

## Setup

To get an interactive development environment run:

    lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

    lein clean

To create a production build run:

    lein do clean, cljsbuild once min

And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL. 

### Cider

To run the project with cider and emacs start a new cider REPL with
`M-x cider-jack-in`.

Once the REPL instance starts, run `(fig-start)` to start figwheel and the
ClojureScript compiler.

Then you can run `(cljs-repl)`, which will attempt to open a socket connection
to the running app. Open your browser at [localhost:3449](http://localhost:3449/)
and you should now have a CLJS REPL.

To stop figwheel run `(fig-stop)`.

## License

[Mozilla Public License Version 2.0](LICENSE)
