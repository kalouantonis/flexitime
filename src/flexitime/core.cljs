(ns flexitime.core
  (:require [reagent.core :as r]
            [clojure.string :as string]
            [goog.string :as gstring]
            [goog.string.format]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; App DB
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defonce app-state (r/atom {}))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def hours-per-day 24)
(def minutes-per-hour 60)
(def minutes-per-day (* hours-per-day 60))
(def zoom-level 0.5)

(defn date->time-str
  [date]
  (str (.getHours date) ":" (.getMinutes date)))

(defn minutes-since-midnight
  "Get the number of minutes from the start of the day for <date>."
  [date]
  (+ (* 60 (.getHours date))
     (.getMinutes date)))

(defn minutes->pixels
  "returns the height of minute(s) in pixels"
  [minutes]
  (* 3 zoom-level minutes))

(defn prevent-default
  "Run event handler F and then prevent default on the event."
  [f]
  (fn [e]
    (f e)
    (.preventDefault e)
    (.stopPropagation e)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Components
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn hours-component
  "Component of each hour in the day."
  []
  [:div#hours
   (for [h (range hours-per-day)]
     [:div#hour {:style {:height (minutes->pixels minutes-per-hour)} :key h}
      (gstring/format "%02d:00" h)])])

(defn activities-component
  "Component of activity overlay."
  []
  [:div#activities
   (for [activity (:activities @app-state)
         :let [[start end :as interval] (:interval activity)]]
     [:div#activity {:style {:top  (minutes->pixels (minutes-since-midnight start))
                             :height (minutes->pixels
                                      (- (minutes-since-midnight end)
                                         (minutes-since-midnight start)))}
                     :key (hash activity)}
      [:span (:name activity)]])])

(defn time-marker-component
  "Component of a marker indicating the current time."
  []
  (r/with-let [now (r/atom (js/Date.))]
    (js/setTimeout #(reset! now (js/Date.)) (* 3 1000))
    [:div#time-marker
     {:style {:top (minutes->pixels (minutes-since-midnight @now))}}]))

(defn selection-component
  [selection]
  (let [height (- (:current-y selection) (:start-y selection))]
    [:div#selection {:style {:top (+ (- (:start-y selection)
                                        (:offset-y selection))
                                     (.min js/Math height 0))
                             :height (.abs js/Math height)}}]))

(defn home-component []
  (r/with-let [drag-state (r/atom {})]
    [:div
     [:h1 (str (.toDateString (js/Date.)))]
     [:div#timeline {:on-mouse-down (prevent-default
                                     (fn [e]
                                       (reset! drag-state
                                               {:start-x   (.-pageX e)
                                                :start-y   (.-pageY e)
                                                :current-x (.-pageX e)
                                                :current-y (.-pageY e)
                                                :offset-x  (.-offsetLeft (.-currentTarget e))
                                                :offset-y  (.-offsetTop  (.-currentTarget e))
                                                :dragging? true})))
                     :on-mouse-move (prevent-default
                                     (fn [e]
                                       (when (:dragging? @drag-state)
                                         (swap! drag-state assoc
                                                :current-x (.-pageX e)
                                                :current-y (.-pageY e)))))
                     :on-mouse-up   (prevent-default
                                     #(swap! drag-state assoc :dragging? false))}
      [hours-component]
      [activities-component]
      [time-marker-component]
      [selection-component @drag-state]]]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn mount-root []
  (r/render [home-component]
            (.getElementById js/document "app")))

;; WARNING: For testing only, do not use anywhere.
(defn time-str->date
  "Creates a date from given time string, assuming the date as today."
  [time-str]
  (let [[h m & _] (string/split time-str #":")]
    (doto (js/Date.)
      (.setHours h)
      (.setMinutes m))))

(defn on-js-reload
  "Called when figwheel triggers a reload."
  [] (mount-root))

(defonce boot!
  (do
    (enable-console-print!)
    (println "Booting app!")
    (swap! app-state assoc :activities
           [{:interval (map time-str->date ["13:00" "14:45"])
             :name "Lunch at grandmas"}
            {:interval (map time-str->date ["14:45" "15:00"])
             :name "Driving"}
            {:interval (map time-str->date ["15:10" "16:10"])
             :name "Practice guitar"}
            {:interval (map time-str->date ["16:15" "17:35"])
             :name "Drive to coffee shop"}
            {:interval (map time-str->date ["17:40" "20:10"])
             :name "Work on flexitime"}
            {:interval (map time-str->date ["20:10" "21:30"])
             :name "Work on scheme interpretter"}
            {:interval (map time-str->date ["21:30" "23:30"])
             :name "Drinks with Alex"}])
    (mount-root)))
