(ns flexitime.test-runner
  (:require [doo.runner :refer-macros [doo-all-tests]]))

(doo-all-tests)
